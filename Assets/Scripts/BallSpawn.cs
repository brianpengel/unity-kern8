﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using FlowEngine.Curves;

public class BallSpawn : NetworkBehaviour {

	[SerializeField] private Transform ballSpawn ;
  [SerializeField] private NetworkedBall ball ;
  [SerializeField] private GameManager gm ;

	public override void OnStartServer () {
    var go = Instantiate (ball, ballSpawn.position, Quaternion.identity) ;
		go.Inject(gm) ;
    NetworkServer.Spawn(go.gameObject) ;
	}

}
