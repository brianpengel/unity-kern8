﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using FlowEngine.Curves;

public class PlayerPath : NetworkBehaviour {

	[SerializeField] private PolygonCollider2D killBox ;
	[SerializeField] private LineRenderer killRenderer ;
	[SerializeField] private LineRenderer renderer ;
	[SerializeField] private Spline path ;
	[SerializeField] private float killBoxOffset = 1 ;

	public void Awake () {
		Init( path.Asset ) ;
	}

	public void Init (SplineData level) {
		path.Asset = level ;

		float step = 1f / (level.Length * 10) ;
		var positions 		= new Vector3[Mathf.RoundToInt(1f / step)] ;
		var killPositions = new Vector3[positions.Length] ;

		for (int t = 0 ; t < positions.Length - 1 ; ++t) {
			positions[t] = path.GetPointAt(t * step).position ;
			killPositions[t] = positions[t] * (1 + killBoxOffset * .1f) ;
		}

		killRenderer.positionCount = renderer.positionCount = positions.Length;
		killPositions[ killPositions.Length - 1 ] = killPositions[0] ;
		positions[ positions.Length - 1 ] = positions[0] ;
		killRenderer.SetPositions(killPositions) ;
		renderer.SetPositions(positions) ;

		killBox.points = killRenderer.GetEdgePoints() ;
	}

}
