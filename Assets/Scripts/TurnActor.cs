﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public abstract class TurnActor : NetworkBehaviour {

  [SyncVar, SerializeField] protected bool activeActor ; // currently your turn
  [SyncVar, SerializeField] protected bool gameStarted ;
  private Coroutine turnRoutine ;

  // ///////////////////// //
  //  Game
  // ///////////////////// //
  public override void OnStartClient() {
		base.OnStartClient();
		TurnManager.Instance.RegisterTurnActor(this);
    Debug.Log("Client Network Player start");
  }

  public override void OnNetworkDestroy() {
		base.OnNetworkDestroy();
		TurnManager.Instance.DeregisterTurnActor(this);
    (CustomNetworkUI.singleton as CustomNetworkUI).EndGame() ;
    Debug.Log("Client Network Player stop");
  }

  [Server]
	public virtual void StartGame () {
		gameStarted = true;
	}


  // ///////////////////// //
  //  Turn
  // ///////////////////// //

  [Server]
	public void OnNewTurn () {
		RpcOnNewTurn();
	}

	[ClientRpc]
	private void RpcOnNewTurn () {
    OnTurnChanged () ;
  }

	// Begin Turn
  [Server]
	public void StartTurn () {
		activeActor = true;
		RpcStartTurn();
	}

	[ClientRpc]
	private void RpcStartTurn() {
    activeActor = true ;
    OnTurnStart () ;
    turnRoutine = StartCoroutine (OnTurn()) ;
  }

  // CMD end turn
  public void EndTurn () {
    CmdEndTurn () ;
  }

  [ClientRpc]
	private void RpcEndTurn() {
    if(turnRoutine != null) {
      StopCoroutine(turnRoutine) ;
      turnRoutine = null ;
    }

    OnTurnEnd () ;
  }

  [Command]
  private void CmdEndTurn () {
    RpcEndTurn () ;
    TurnManager.Instance.EndTurn (this) ;
    activeActor = false ;
  }

  protected abstract IEnumerator OnTurn () ;
  protected abstract void OnTurnStart () ;
  protected abstract void OnTurnEnd () ;
  protected virtual void OnTurnChanged () {}

  // [Client] protected abstract IEnumerator OnTurn () ;
  // [Client] protected abstract void OnTurnStart () ;
  // [Client] protected abstract void OnTurnEnd () ;


}
