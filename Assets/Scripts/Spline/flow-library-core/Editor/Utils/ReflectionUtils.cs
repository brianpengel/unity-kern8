using System.Collections.Generic ;
using System.Reflection ;
using System.Linq ;
using System ;

namespace FlowEditor {

  public static class ReflectionUtils {

    /* GetSubclassesOf <T>
     * bool allowAbstract = false, bool requireParameterlessConstructor = false
     *
     * Desc.
     * Returns a List of subclasses with a default filter.
     *
     * Ret. List<Type>
     */
    public static List<Type> GetSubclassesOf <T> (bool allowAbstract = false, bool requireParameterlessConstructor = false) {
      return GetSubclassesOf <T> (t =>
        (allowAbstract || !t.IsAbstract) &&
        (!requireParameterlessConstructor || t.GetConstructor(Type.EmptyTypes) != null)
      ) ;
    }

    /* GetSubclassesOf <T>
     * Func<Type, bool> filter
     *
     * Desc.
     * Returns a List of subclasses filtered by delegate filter.
     *
     * Ret. List<Type>
     */
    public static List<Type> GetSubclassesOf <T> (Func<Type, bool> filter) {
      Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies () ;
      var types = new List<Type> ();
      var genericType = typeof(T) ;

      for(int i = 0; i < assemblies.Length; i ++) {
        Type[] foundTypes = assemblies[i].GetTypes()
          .Where( t => (t.IsSubclassOf(genericType) || genericType.IsAssignableFrom(t)) && filter(t) ).ToArray() ;

        for(int j = 0; j < foundTypes.Length; j ++)
          types.Add(foundTypes[j]) ;
      }

      return types ;
    }
  }
}
