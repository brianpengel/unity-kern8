namespace FlowEditor {

  [System.Serializable]
  public abstract class PreferenceContainer : FlowEngine.DataStoreContainer {

    public void Store () {
      PreferenceEditor.StorePreferences(this) ;
    }

  }
}
