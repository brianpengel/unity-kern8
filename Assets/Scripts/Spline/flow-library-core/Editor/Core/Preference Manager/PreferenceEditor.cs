using System.Collections.Generic ;
using System.Linq ;
using System.IO ;
using System ;

using UnityEditor ;
using UnityEngine ;
using FlowEngine ;


namespace FlowEditor {

  [InitializeOnLoad]
  public sealed class PreferenceEditor : Editor {

    // ///////// //
    //  Statics
    // ///////// //
    private static readonly string DATA_ROOT = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
    private static readonly string DATA_DIRECTORY = Path.Combine(DATA_ROOT, "Unity-Flow") ;
    private static readonly string PREFERENCE_FILE = "Preferences.json" ;
    private static readonly DataStore PREFERENCE_STORE ;


    static PreferenceEditor () {
      PREFERENCE_STORE = DataStore.LoadAtPath(DATA_DIRECTORY, PREFERENCE_FILE) ;
    }

    public static T GetPreferences <T> (string id) where T : PreferenceContainer, new () {
      if(!PREFERENCE_STORE.ContainsKey(id))
        PREFERENCE_STORE.Add(id, new T()) ;
      return PREFERENCE_STORE[id] as T ;
    }

    public static void RemovePreferences (string id) {
      PREFERENCE_STORE.Remove (id) ;
      DataStore.Save (PREFERENCE_STORE) ;
    }

    internal static void StorePreferences (PreferenceContainer container) {
      // Get key of container
      if(!PREFERENCE_STORE.ContainsValue(container))
        throw new Exception("Preference container is not being stored.") ;
      DataStore.Save ( PREFERENCE_STORE ) ;
    }

  }
}
