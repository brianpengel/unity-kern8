using UnityEngine ;
using FlowEngine ;
using System ;

namespace FlowEditor {

  public static class FlowStyles {

    // Default List styles
    public static readonly GUIStyle LIST_HEADER        = (GUIStyle) "RL Header";
    public static readonly GUIStyle LIST_DRAG_HANDLE   = (GUIStyle) "RL DragHandle";
    public static readonly GUIStyle LIST_BACKGROUND    = (GUIStyle) "RL Background";
    public static readonly GUIStyle LIST_ELEMENT       = (GUIStyle) "RL Element";
    public static readonly GUIStyle LIST_FOOTER        = (GUIStyle) "RL Footer";
    public static readonly GUIStyle LIST_FOOTER_BUTTON = (GUIStyle) "RL FooterButton";

    // private const int buttonWidth = 25;
    // public const int padding = 6;
    // public const int dragHandleWidth = 20;


    // Default Toolbar Icons
    public const string ICON_TOOLBAR_PLUS       = "Toolbar Plus" ;
    public const string ICON_TOOLBAR_PLUS_MORE  = "Toolbar Plus More" ;

    public const string ICON_TOOLBAR_MINUS      = "Toolbar Minus" ;
    public const string ICON_TOOLBAR_MINUS_MORE = "Toolbar Minus More" ;


    // public readonly GUIStyle preButton = (GUIStyle) "RL FooterButton";

  }
}
