using UnityEditor ;
using UnityEngine ;
using System.Collections.Generic ;
using System ;

using FlowEngine ;


namespace FlowEditor {

  [InitializeOnLoad, CustomEditor(typeof(AssetProxy), true)]
  internal class AssetProxyEditor : Editor {

    private SerializedProperty asset;

    public void OnEnable () {
      if(target == null) return ;
      asset = serializedObject.FindProperty("target");
    }

    public override void OnInspectorGUI() {
      if(asset.objectReferenceValue != null) return ;

      EditorGUILayout.Space() ;
      EditorGUILayout.BeginVertical(EditorStyles.helpBox) ;
        EditorGUILayout.LabelField(new GUIContent("Asset Missing")) ;
        EditorGUILayout.PropertyField(asset, new GUIContent("Assign:"));
        serializedObject.ApplyModifiedProperties();
      EditorGUILayout.EndVertical() ;
      EditorGUILayout.Space() ;
    }


    // ///////////////// //
    //  Statics
    // ///////////////// //
    private static readonly Dictionary<Type, Type> proxyTypes ;


    static AssetProxyEditor () {
      proxyTypes = GetProxyTypes () ;
      SceneView.onSceneGUIDelegate += s=>CreateProxiesForSelection() ;
      EditorApplication.hierarchyWindowItemOnGUI = (i, r)=>CreateProxiesForSelection() ;
    }

    /* GetProxyTypes
     *
     * Desc.
     * Gets all non-abstract subclasses of class (AssetProxy) with a
     * (TypeTargetAttribute) attribute
     *
     * Ret. Dictionary<Type, Type>
     */
    private static Dictionary<Type, Type> GetProxyTypes () {
      var sortedTypes = new Dictionary<Type, Type> () ;
      var types = ReflectionUtils.GetSubclassesOf<AssetProxy> (t => {
        return !t.IsAbstract && t.IsDefined(typeof(TypeTargetAttribute), false) ;
      }) ;

      foreach (var t in types)
        foreach (TypeTargetAttribute attr in t.GetCustomAttributes(typeof(TypeTargetAttribute), false))
          if(!sortedTypes.ContainsKey(attr.target))
            sortedTypes.Add(attr.target, t) ;

      return sortedTypes ;
    }

    /* CreateProxiesForSelection
     *
     * Desc.
     * Creates GameObjects with AssetProxy component
     * for selection.
     *
     * TODO: prevent cursor change when selection 
     * doesn't contain a type with a proxy.
     */
    private static void CreateProxiesForSelection () {
      // On drag
      if(Event.current.type == EventType.DragUpdated)
        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

      // On drag release
      else if(Event.current.type == EventType.DragPerform) {
        var selection = new List<GameObject>();
        DragAndDrop.AcceptDrag();

        // For dragged objects
        foreach (var assetReference in DragAndDrop.objectReferences) {
          // Check if object of supported type
          string path = AssetDatabase.GetAssetPath(assetReference) ;
          var type = AssetDatabase.GetMainAssetTypeAtPath(path) ;
          if(  type == null || !proxyTypes.ContainsKey(type) ) continue ;

          // Create proxy
          var proxy = AssetUtils.CreateProxy(type.Name, assetReference, proxyTypes[type]) ;
          selection.Add(proxy.gameObject);
        }

        // Set selection
        Selection.objects = selection.ToArray();
        if(selection.Count > 0) Event.current.Use() ;
      }
    }

  }
}
