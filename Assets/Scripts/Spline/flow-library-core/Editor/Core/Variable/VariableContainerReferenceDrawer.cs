using UnityEditor ;
using UnityEngine ;
using FlowEngine ;


namespace FlowEditor {

  [CustomPropertyDrawer(typeof(VariableContainerReference), true)]
  internal class VariableContainerReferenceDrawer : PropertyDrawer {

    private static readonly string[] POPUP_OPTIONS = new string[]{ "Use local value", "Use reference" };
    private static readonly GUIStyle POPUP_STYLE = (GUIStyle) "PaneOptions" ;

    static VariableContainerReferenceDrawer () {
      POPUP_STYLE.imagePosition = ImagePosition.ImageOnly;
    }


    SerializedProperty useReference ;
    SerializedProperty localValue ;
    SerializedProperty variable ;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
      // Variables
      useReference = property.FindPropertyRelative("useReference") ;
      localValue = property.FindPropertyRelative("localValue") ;
      variable = property.FindPropertyRelative("reference") ;


      EditorGUI.BeginChangeCheck();

      label = EditorGUI.BeginProperty(position, label, property);
      position = EditorGUI.PrefixLabel(position, label);

      // Calculate rect for configuration button
      Rect buttonRect = new Rect(position);
      buttonRect.yMin += POPUP_STYLE.margin.top;
      buttonRect.width = POPUP_STYLE.fixedWidth + POPUP_STYLE.margin.right;
      position.xMin = buttonRect.xMax;

      // Store old indent level and set it to 0, the PrefixLabel takes care of it
      int indent = EditorGUI.indentLevel;
      EditorGUI.indentLevel = 0;

      int index = useReference.boolValue ? 1 : 0,
          result = EditorGUI.Popup(buttonRect, index, POPUP_OPTIONS, POPUP_STYLE);

      useReference.boolValue = result == 1 ;

      EditorGUI.PropertyField(position,
        useReference.boolValue ? variable : localValue,
        GUIContent.none);

      if (EditorGUI.EndChangeCheck())
        property.serializedObject.ApplyModifiedProperties();

      EditorGUI.indentLevel = indent;
      EditorGUI.EndProperty();
    }

  }
}
