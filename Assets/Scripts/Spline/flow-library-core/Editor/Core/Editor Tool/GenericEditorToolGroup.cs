using UnityEditor ;
using UnityEngine ;
using System ;

namespace FlowEditor {

  public abstract class EditorToolGroup <T> : Editor where T : class {

    private const int NO_TOOL_INDEX = -1 ;

    private EditorTool<T> [] tools ;
    private int activeTool = NO_TOOL_INDEX ;

    public void OnEnable () {
      if(target == null) return ;
      tools = CreateTools () ;
    }

    public override void OnInspectorGUI() {
      EditorGUILayout.BeginVertical() ;
        EditorGUILayout.Space() ;

          if(tools.Length == 0)
            DrawNoToolGUI () ;

          else {
            // Clamp activeTool value
            activeTool = Mathf.Clamp(activeTool, NO_TOOL_INDEX, tools.Length - 1) ;

            // Draw GUI
            DrawHeaderBar() ;
            if(activeTool == NO_TOOL_INDEX)
                 DrawNoToolGUI () ;
            else tools[activeTool].OnInspectorGUI () ;
          }

        EditorGUILayout.EndVertical() ;
      EditorGUILayout.Space() ;
    }


    public void OnSceneGUI () {
      var colour = Handles.color ;

      if(activeTool != NO_TOOL_INDEX) {
        Tools.current = Tool.None;
        tools[activeTool].DrawSceneTool() ;
      }

      Handles.color = colour ;
    }

    /* DrawNoToolGUI
     *
     * Desc.
     * Draws GUI when no tool is selected
     *
     */
    private void DrawNoToolGUI () {
      EditorGUILayout.Space() ;
      EditorGUILayout.BeginVertical(EditorStyles.helpBox) ;
        EditorGUILayout.LabelField(new GUIContent("No tool selected")) ;
        EditorGUILayout.LabelField(new GUIContent("Please select a tool")) ;
      EditorGUILayout.EndVertical() ;
      EditorGUILayout.Space() ;
    }


    /* DrawHeaderBar
     *
     * Desc.
     * Draws header buttons
     *
     */
    private void DrawHeaderBar () {
      EditorGUILayout.Space() ;
      EditorGUILayout.BeginHorizontal ();
        GUILayout.FlexibleSpace() ;

        for(int cnt = 0; cnt < tools.Length; cnt ++) {
          GUIStyle style = EditorStyles.miniButtonMid ;
          if(cnt == 0 && cnt == tools.Length - 1)
            style = EditorStyles.miniButton ;

          else if(cnt == 0)
            style = EditorStyles.miniButtonLeft ;

          else if(cnt == tools.Length - 1)
            style = EditorStyles.miniButtonRight ;

          activeTool = DrawHeaderToggle(activeTool, cnt, tools[cnt].inactiveIcon, tools[cnt].activeIcon, tools[cnt].toolTip, style) ;
        }

        GUILayout.FlexibleSpace() ;
      EditorGUILayout.EndHorizontal ();
      EditorGUILayout.Space() ;
    }

    /* DrawHeaderToggle
     * int activeIndex, int index, string iconID, string pressedIcon, string tooltip, GUIStyle style
     *
     * Desc.
     * Renders a single button of the header buttons using (Guistyle) style
     *
     * Ret. int
     */
    private int DrawHeaderToggle (int activeIndex, int index, string iconID, string pressedIcon, string tooltip, GUIStyle style) {
      if(pressedIcon == String.Empty) pressedIcon = iconID ;

      bool original = activeIndex == index ;

      var iconLink = (original ? pressedIcon : iconID) ;
      GUIContent icon = iconLink == "" ? GUIContent.none : EditorGUIUtility.IconContent(iconLink) ;
      var content = new GUIContent(icon) ;
      content.tooltip = tooltip ;

      bool toggle = GUILayout.Toggle(original, content, style) ;

      if(toggle != original) {
        SceneView.RepaintAll() ;
        return original ? NO_TOOL_INDEX : index ;
      }

      return activeIndex ;
    }

    /* CreateTools
     *
     * Desc.
     * Creates a array of UnityEditor.Editors of type (EditorTool<T>)
     *
     * Ret. EditorTool<T>[]
     */
    private EditorTool<T>[] CreateTools () {
      var t = new EditorTool<T>[AVAILABLE_TOOLS.Length] ;

      for(int cnt = 0 ; cnt < AVAILABLE_TOOLS.Length ; cnt ++)
        t[cnt] = Editor.CreateEditor(serializedObject.targetObject, AVAILABLE_TOOLS[cnt]) as EditorTool<T> ;

      return t ;
    }


    // ///////////////// //
    //  Statics
    // ///////////////// //
    private static readonly Type[] AVAILABLE_TOOLS ;

    static EditorToolGroup () {
      AVAILABLE_TOOLS = GetAvailableTools () ;
    }


    /* GetAvailableTools
     *
     * Desc.
     * Gets all non-abstract subclasses of class (EditorTool<T>) with a
     * (TypeTargetAttribute) attribute
     *
     * Ret. Type[]
     */
    private static Type[] GetAvailableTools () {
      return ReflectionUtils.GetSubclassesOf<EditorTool<T>> (t => {
        return !t.IsAbstract /*&& t.IsDefined(typeof(TypeTargetAttribute), false)*/ ;
      }).ToArray() ;
    }

  }

}
