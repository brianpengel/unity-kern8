using System ;

[AttributeUsage(AttributeTargets.Class)]
public class TypeTargetAttribute : Attribute {

  public Type target { get ; private set ; }

  public TypeTargetAttribute (Type target) {
    this.target = target ;
  }

}
