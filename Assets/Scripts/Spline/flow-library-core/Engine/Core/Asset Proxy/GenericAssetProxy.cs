
namespace FlowEngine {

  public abstract class AssetProxy<T> : AssetProxy where T : UnityEngine.Object {

    new public T Asset {
      get { return target == null ? null : (T)target ; }
      set { target = value ; }
    }

  }
}
