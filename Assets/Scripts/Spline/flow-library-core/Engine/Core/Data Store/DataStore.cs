using System.Collections.Generic ;
using System.IO ;
using System ;

using UnityEngine ;


namespace FlowEngine {

  [System.Serializable, CreateAssetMenuAttribute(menuName = "Flow/DataStore", fileName = "DataStore")]
  public sealed class DataStore : ScriptableObject, ISerializationCallbackReceiver {

    [SerializeField, HideInInspector] private List<SerializableType> serializedTypes ;
    [SerializeField, HideInInspector] private List<string> serializedContainers ;
    [SerializeField, HideInInspector] private List<string> indexer ;

    private string baseDirectory ;
    private string path ;

    // Store containers
    private List<DataStoreContainer> containerStore ;


    public DataStoreContainer this[string key] {
      get {
        int i = indexer.IndexOf(key) ;
        return i == -1 ? null : containerStore[i] ;
      }
    }

    public string FilePath {
      set { path = value ; }
      get { return path ; }
    }

    void OnEnable () {
      if(containerStore == null) {
        baseDirectory  = Application.persistentDataPath ;
        containerStore = new List<DataStoreContainer> () ;
        indexer        = new List<string> () ;
      }
    }

    /* Add
     * string id, DataStoreContainer container
     *
     * Desc.
     * Adds container (DataStoreContainer) with id (string) to DataStore.
     *
     */
    public void Add (string id, DataStoreContainer container) {
      int index = indexer.IndexOf(id) ;
      if(index == -1) {
        containerStore.Add(container) ;
        indexer       .Add(id) ;
      }

      else containerStore [index] = container ;
    }


    /* Remove
     * DataStoreContainer container
     *
     * Desc.
     * Removes container (DataStoreContainer) from DataStore.
     *
     */
    public void Remove (DataStoreContainer container) {
      Remove( containerStore.IndexOf(container) ) ;
    }


    /* Remove
     * DataStoreContainer container
     *
     * Desc.
     * Removes (DataStoreContainer) from DataStore with id (string).
     *
     */
    public void Remove (string id) {
      Remove( indexer.IndexOf(id) ) ;
    }

    private void Remove (int i) {
      if(i != -1) {
        containerStore.RemoveAt(i) ;
        indexer       .RemoveAt(i) ;
      }
    }


    /* ContainsValue
     * DataStoreContainer container
     *
     * Desc.
     * Checks if DataStore contains container (DataStoreContainer)
     *
     * Ret. bool
     */
    public bool ContainsValue (DataStoreContainer container) {
      return containerStore.Contains (container) ;
    }


    /* ContainsValue
     * string key
     *
     * Desc.
     * Checks if DataStore contains container with key (string)
     *
     * Ret. bool
     */
    public bool ContainsKey (string key) {
      return indexer.Contains (key) ;
    }


    // //////////////////////////////// //
    //  ISerializationCallbackReceiver
    // //////////////////////////////// //
    public void OnBeforeSerialize () {
      serializedContainers = new List<string> () ;
      serializedTypes = new List<SerializableType> () ;

      foreach(var container in containerStore) {
        serializedContainers.Add(JsonUtility.ToJson(container)) ;
        serializedTypes.Add(new SerializableType(container.GetType ())) ;
      }
    }

    public void OnAfterDeserialize () {
      containerStore = new List<DataStoreContainer> () ;

      for(int cnt = 0 ; cnt < indexer.Count ; cnt ++)
        containerStore.Add(JsonUtility.FromJson (
          serializedContainers[cnt],
          serializedTypes[cnt]) as DataStoreContainer
        ) ;
    }

    // ///////// //
    //  Statics
    // ///////// //
    public static void Save (DataStore store) {
      string fullPath = Path.Combine(store.baseDirectory, store.path);
      string dir = Path.GetDirectoryName(fullPath) ;

      if (!Directory.Exists(dir))
        Directory.CreateDirectory(dir);

      string content = JsonUtility.ToJson(store) ;
      File.WriteAllText(fullPath, content);
    }


    public static DataStore Load (string path) {
      return LoadAtPath (Application.persistentDataPath, path) ;
    }


    public static DataStore LoadAtPath (string baseDir, string path) {
      string fullPath = Path.Combine(baseDir, path) ;
      DataStore store = null ;

      if (File.Exists(fullPath))
        try {
          string json = File.ReadAllText(fullPath) ;
          store = JsonUtility.FromJson <DataStore> (json) ;
        }

        catch(Exception) {}

      if(store == null)
        store = ScriptableObject.CreateInstance <DataStore> () ;
      store.baseDirectory = baseDir ;
      store.path = path ;
      return store ;
    }

  }
}
