using UnityEngine ;
using System ;

namespace FlowEngine {

  public interface ISubscribable {

    void RemoveListener (Action deleg) ;
    void AddListener (Action deleg) ;

  }

  public interface ISubscribable <T> {

    void RemoveListener (Action<T> deleg) ;
    void AddListener (Action<T> deleg) ;

  }

  public interface ISubscribableGeneric {

    void RemoveListener<T> (Action<T> deleg) ;
    void AddListener<T> (Action<T> deleg) ;

  }
}
