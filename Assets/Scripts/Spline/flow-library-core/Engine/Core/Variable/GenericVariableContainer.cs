using UnityEngine ;
using System ;

namespace FlowEngine {

  public abstract class VariableContainer<T> : VariableContainer, ISubscribable<T> {

    [SerializeField] new private T initialValue, value ;

    new public T Value {
      set { SetValue( value ) ; }
      get { return this.value ; }
    }

    private void OnEnable () {
      value = initialValue ;
    }

    private void SetValue (T val) {
      BroadCaster<VariableContainer<T>, T>.Cast (this, val) ;
      value = val ;
    }

    public static explicit operator T(VariableContainer<T> b) {
      return b.value ;
    }

    // ////////////////// //
    //  ISubscribable<T>
    // ////////////////// //
    public void RemoveListener(Action<T> deleg) {
      BroadCaster<VariableContainer<T>, T>.RemoveListener (this, deleg) ;
    }

    public void AddListener(Action<T> deleg) {
      BroadCaster<VariableContainer<T>, T>.AddListener (this, deleg) ;
    }

  }
}
