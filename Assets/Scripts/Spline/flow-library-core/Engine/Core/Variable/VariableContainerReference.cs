using UnityEngine ;

namespace FlowEngine {

  public abstract class VariableContainerReference {

    [SerializeField] protected object reference, localValue ;
    [SerializeField] protected bool useReference ;

    public object Reference { get; set; }
    public object Value     { get; set; }
  }
}
