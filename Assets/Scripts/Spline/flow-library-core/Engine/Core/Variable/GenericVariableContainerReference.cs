using UnityEngine ;

namespace FlowEngine {

  public abstract class VariableContainerReference<T, U> : VariableContainerReference where U : VariableContainer<T> {

    [SerializeField] new private U reference ;
    [SerializeField] new private T localValue ;


    new public U Reference {
      set { reference = value ; }
      get { return reference  ; }
    }

    new public T Value {
      set {
        if(UseRef())
             reference.Value = value ;
        else localValue = value ;
      }

      get {
        if(UseRef())
             return reference.Value ;
        else return localValue ;
      }
    }

    private bool UseRef () {
      return reference != null && useReference ;
    }

  }
}
