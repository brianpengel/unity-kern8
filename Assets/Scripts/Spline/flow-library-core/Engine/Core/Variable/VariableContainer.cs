using UnityEngine ;
using System ;

namespace FlowEngine {

  public abstract class VariableContainer : ScriptableObject, ISubscribable<object> {

    [SerializeField] protected object initialValue, value ;

    public object Value {
      set { SetValue( value ) ; }
      get { return this.value ; }
    }

    private void SetValue (object val) {
      BroadCaster<VariableContainer, object>.Cast (this, val) ;
      value = val ;
    }


    // /////////////////////// //
    //  ISubscribable<object>
    // /////////////////////// //
    public void RemoveListener(Action<object> deleg) {
      BroadCaster<VariableContainer, object>.RemoveListener (this, deleg) ;
    }

    public void AddListener(Action<object> deleg) {
      BroadCaster<VariableContainer, object>.AddListener (this, deleg) ;
    }

  }
}
