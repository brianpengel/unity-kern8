using System.Collections.Generic ;
using System.Collections ;
using System ;
using UnityEngine ;

namespace FlowEngine.Curves {

  [TypeTargetAttribute(typeof(SplineData))]
  public class Spline : AssetProxy<SplineData>, ICurve {

    // #if UNITY_EDITOR
      // Editor only values
      [SerializeField] private Color renderColour = Color.red ;
      [SerializeField] private bool isVisible = true ;

      public Color RenderColour { get { return renderColour ; } }
      public bool IsVisible     { get { return isVisible ; } }
    // #endif

    // ///////// //
    // ICurve
    // ///////// //
    public TransformPoint[] Points { get { return Array.ConvertAll(Asset.Points, p => p.ApplyTransformation(transform)) as TransformPoint[] ; } }
    public TransformPoint Begin    { get { return Asset.Begin.ApplyTransformation(transform) ; } }
    public TransformPoint End      { get { return Asset.End.ApplyTransformation(transform) ; } }

    public float Length            { get { return Asset.Length ; } }
    public int Count               { get { return Asset.Count  ; } }


    public void SetPoint (int i, TransformPoint p) {
      Asset.SetPoint(i, p.InverseTransformation(transform)) ;
    }

    public TransformPoint GetPointAt (float t) {
      return Asset.GetPointAt(t).ApplyTransformation(transform) ;
    }

    public Vector3 GetNormalAt (float t, Vector3 up) {
      return Asset.GetNormalAt(t, up) ;
    }

    public Vector3 GetTangentAt (float t) {
      return Asset.GetTangentAt(t) ;
    }

    public Vector3 GetDerivativeAt (float t) {
      return Asset.GetDerivativeAt(t) ;
    }

  }

}
