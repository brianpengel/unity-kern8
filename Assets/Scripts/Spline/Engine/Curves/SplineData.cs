using System.Collections.Generic ;
using System.Collections ;
using System ;
using UnityEngine ;

namespace FlowEngine.Curves {

  [System.Serializable, CreateAssetMenuAttribute]
  public sealed class SplineData : ScriptableObject, ISpline<CurveBehaviour>, ISerializationCallbackReceiver {

    [SerializeField] private List<SerializableType> curveTypes ;
    [SerializeField] private List<CurveBehaviour>   curves ;
    [SerializeField] private List<TransformPoint>   points ;

    [SerializeField] private float[] indexedLength ;
    [SerializeField] private int  [] indexedPointIndices ;

    [SerializeField] private float fullLength = 0 ;


    public TransformPoint[] Points { get { return points.ToArray()       ; } }
    public TransformPoint Begin    { get { return curves[0].Begin        ; } }
    public TransformPoint End      { get { return curves[Count - 1].End  ; } }

    public float Length            { get { return fullLength         ; } }
    public int Count               { get { return curves.Count       ; } }



    void OnEnable () {
      if(curves == null)
        Clear() ;
      UpdateIndexedValues() ;

      // Debug.Log("// ///////////////////////////////// //") ;
      // var m = (float offs, float prev, float curr) = {
      //   float len = curr - prev ;
      //   float a = 1f / len ;
      //   return (offs - prev) * a ;
      // } ;
      //
      // Debug.Log("// ///////////////////////////////// //") ;
    }

    private void InsertCurve (int i, CurveBehaviour c) {
      curveTypes.Insert(i, c.GetType()) ;
      curves    .Insert(i, c) ;

      int pointIndex = indexedPointIndices.Length == 0 ? 0 : indexedPointIndices[i] ;
      points.InsertRange(pointIndex, (TransformPoint[]) c) ;

      UpdateIndexedValues () ;
    }

    private void RemoveCurveAt (int i) {
      int l = curves[i].Count ;
      curveTypes.RemoveAt(i) ;
      curves    .RemoveAt(i) ;

      int pointIndex = indexedPointIndices.Length == 0 ? 0 : indexedPointIndices[i] ;
      points.RemoveRange(pointIndex, l - 1) ;

      UpdateIndexedValues() ;
    }

    private void ReplaceCurve () {

    }

    // ////////////////// //
    // ISpline<CurveBehaviour>
    // ////////////////// //
    public void AddPoint(Vector3 p) {
      // curves.Add(c) ;
      UpdateIndexedValues () ;
    }

    public void RemoveAt(int i) {
      // curves.Remove(c) ;
      RemoveCurveAt (GetCurveIndex(i)) ;
      UpdateIndexedValues () ;
    }

    // ///////// //
    // ICurve
    // ///////// //
    public void SetPoint (int i, TransformPoint p) {
      points[i] = p ;

      var j = GetCurveIndex(ref i) ;
      curves[j].SetPoint(i, p) ;
      UpdateIndexedValues () ;
    }

    public Vector3 GetNormalAt (float t, Vector3 up) {
      return curves[GetCurveIndex(ref t)].GetNormalAt(t, up) ;
    }

    public Vector3 GetTangentAt (float t) {
      return curves[GetCurveIndex(ref t)].GetTangentAt(t) ;
    }

    public TransformPoint GetPointAt (float t) {
      var u = t ;
      var c = GetCurveIndex(ref t) ;
      // Debug.Log ( u + " =  Curve: " + c + ", Point: " + t) ;
      return curves[c].GetPointAt(t) ;
    }

    public Vector3 GetDerivativeAt (float t) {
      return curves[GetCurveIndex(ref t)].GetDerivativeAt(t) ;
    }

    private void UpdateIndexedValues () {
      float[] lengths    = new float [Count + 1] ;
      int  [] indices    = new int   [Count + 1] ;
      float   fullLength = 0 ;

      for (int cnt = 0 ; cnt < Count; cnt ++)
        fullLength += curves[cnt].Length ;

      // TODO: Find way to integrate in first loop ?
      float step = 1f / fullLength, prevLength = 0 ;
      int prevIndex = 0 ;

      for (int cnt = 0 ; cnt < Count; cnt ++) {
        lengths[cnt] = prevLength ;
        indices[cnt] = prevIndex ;
        prevLength += curves[cnt].Length * step ;
        prevIndex += curves[cnt].Count ;
      }

      lengths[lengths.Length - 1] = prevLength ;
      indices[indices.Length - 1] = prevIndex ;

      this.indexedPointIndices = indices ;
      this.indexedLength = lengths ;
      this.fullLength = fullLength ;
    }


    // ///////// //
    // Helpers
    // ///////// //
    // TODO: Find better name
    public int GetCurveIndex (ref float t) {
      int index = indexedLength.Length - 2 ;
      t = Mathf.Clamp01(t) ;

  		for(; index > 0 ; index --)
  			if(t >= indexedLength[index])
  				break ;

      var prev = indexedLength[index] ;
      var curr = indexedLength[index + 1] ;

      t = (t - prev) * (1f / (curr - prev)) ;
  		return index ;
    }

    private int GetCurveIndex (float t) {
      return GetCurveIndex(ref t) ;
    }


    private int GetCurveIndex (ref int i) {
      int index = indexedPointIndices.Length - 1 ;

    	for(; index > 0 ; index --)
  			if(i >= indexedPointIndices[index])
  				break ;

      i -= indexedPointIndices[index] ;
      return index ;
    }

    private int GetCurveIndex (int i) {
      return GetCurveIndex(ref i) ;
    }

    // ////////////////////////////// //
    // ISerializationCallbackReceiver
    // ////////////////////////////// //
    public void OnAfterDeserialize() {
      if(curveTypes == null)
        curveTypes = new List<SerializableType> () ;

      curves = new List<CurveBehaviour> () ;
      for(int cnt = 0, i = 0 ; cnt < curveTypes.Count ; cnt ++) {
        var curve = (CurveBehaviour) System.Activator.CreateInstance((System.Type)curveTypes[cnt], Vector3.zero, Vector3.up) ;

        for(int j = 0 ; j < curve.Count ; j ++, i ++)
          curve.SetPoint(j, points[i]) ;
        curves.Add(curve) ;
      }
    }

    public void OnBeforeSerialize() {
      if(curves == null)
        curves = new List<CurveBehaviour> () ;

      curveTypes = new List<SerializableType> () ;
      for(int cnt = 0 ; cnt < curves.Count ; cnt ++)
        curveTypes.Add(curves[cnt].GetType()) ;
    }


    // ////////////////////////////// //
    //  ICollection<CurveBehaviour>
    // ////////////////////////////// //
    public bool IsReadOnly { get { return false ; } }

    public void Add(CurveBehaviour c) {
      InsertCurve(curves.Count, c) ;
    }

    public void Clear() {
      this.curveTypes.Clear () ;
      this.curves    .Clear () ;
      this.points    .Clear () ;

      this.indexedPointIndices = null ;
      this.indexedLength = null ;
      this.fullLength = 0 ;
    }

    public bool Contains(CurveBehaviour c) {
      return curves.Contains(c) ;
    }

    public void CopyTo(CurveBehaviour[] c, Int32 i) {
      curves.CopyTo(c, i) ;
    }

    IEnumerator IEnumerable.GetEnumerator() {
      return ((IEnumerable<CurveBehaviour>)this).GetEnumerator() ;
    }

    IEnumerator<CurveBehaviour> IEnumerable<CurveBehaviour>.GetEnumerator() {
      var enumerator = curves.GetEnumerator();
      while (enumerator.MoveNext())
        yield return enumerator.Current ;
    }

    public bool Remove(CurveBehaviour c) {
      int i = curves.IndexOf(c) ;
      if(i == -1) return false ;
      RemoveCurveAt(i) ;
      return true ;
    }

  }
}
