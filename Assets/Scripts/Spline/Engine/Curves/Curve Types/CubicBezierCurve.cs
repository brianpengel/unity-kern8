using UnityEngine ;

namespace FlowEngine.Curves {

  [System.Serializable]
  public class CubicBezierCurve : CurveBehaviour {

    private const int POINT_COUNT = 4 ;

    public CubicBezierCurve (TransformPoint start, TransformPoint end) : base (start, end) {}
    public CubicBezierCurve (Vector3 start, Vector3 end) : base (start, end) {}



    protected override TransformPoint[] CreatePoints (TransformPoint start, TransformPoint end) {
      var p = new TransformPoint[4] ;

      float step = 1f / (POINT_COUNT - 1) ;
      for(int cnt = 0; cnt < POINT_COUNT ; cnt ++)
        p[cnt] = new TransformPoint(Vector3.Lerp(start.position, end.position, step * cnt), Quaternion.identity) ;

      return p ;
    }

    public override TransformPoint GetPointAt(float t) {
      float a = 1f - t ;
      Vector3 p0 = points[0].position,  p1 = points[1].position,
              p2 = points[2].position,  p3 = points[3].position;

      return new TransformPoint(
        a * a * a * p0 + 3f * a * a * t * p1 + 3f * a * t * t * p2 + t * t * t * p3,
        Quaternion.identity
      );
    }


    public override Vector3 GetDerivativeAt (float t) {
      float a = 1f - t ;
      Vector3 p0 = points[0].position,  p1 = points[1].position,
              p2 = points[2].position,  p3 = points[3].position;

  		return 3f * a * a * (p1 - p0) + 6f * a * t * (p2 - p1) + 3f * t * t * (p3 - p2);
  	}
  }
}
