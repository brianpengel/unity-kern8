// using UnityEngine ;
//
// namespace FlowEngine.Curves {
//
//   [System.Serializable]
//   public abstract class CurveData : ScriptableObject, ICurve {
//
//     // Constants
//     private const float LENGTH_STEP = 1f / (CURVE_RESOLUTION - 1) ;
//     private const int CURVE_RESOLUTION = 10 ;
//
//     // Variables
//     [SerializeField] private TransformPoint[] points ;
//     [SerializeField] private float length ;
//
//     public TransformPoint Begin { get { return points[0] ; } }
//     public TransformPoint End   { get { return points[Count] ; } }
//     public float Length         { get { return length ; } }
//     public int Count            { get { return points.Length ; } }
//
//     // Why no Ctor's in ScriptableObjects ?
//     public virtual void Initiate (Vector3 start, Vector3 end) {
//       points = new TransformPoint[2] {
//         new TransformPoint(start, Quaternion.identity),
//         new TransformPoint(  end, Quaternion.identity)
//       } ;
//
//       UpdateLength () ;
//     }
//
//     public void SetPoint (int i, TransformPoint p) {
//       points[i] = p ;
//       UpdateLength () ;
//     }
//
//     public Vector3 GetNormalAt (float t, Vector3 up) {
//       Vector3 tangent = GetTangentAt(t),
//               binormal = Vector3.Cross(up, tangent).normalized ;
//   		return Vector3.Cross(tangent, binormal) ;
//     }
//
//     public Vector3 GetTangentAt (float t) {
//       return GetDerivativeAt(t).normalized;
//     }
//
//     protected void UpdateLength () {
//       Vector3 prev = points[0].position ;
//       float newLength = 0f ;
//
//       for (int cnt = 1; cnt < CURVE_RESOLUTION; cnt++) {
//         Vector3 current = GetPointAt(LENGTH_STEP * cnt).position ;
//         newLength += Vector3.Distance(current, prev) ;
//         prev = current ;
//       }
//
//       length = newLength ;
//     }
//
//     // /////////// //
//     //  Abstracts
//     // /////////// //
//     public abstract TransformPoint GetPointAt  (float t) ;
//     public abstract Vector3 GetDerivativeAt    (float t) ;
//
//   }
// }
