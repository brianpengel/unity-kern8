using System.Collections.Generic ;
using System ;
using UnityEngine ;

namespace FlowEngine.Curves {

  [System.Serializable]
  public abstract class CurveBehaviour : ICurve {

    // Constants
    private const float LENGTH_STEP = 1f / CURVE_RESOLUTION ;
    private const int CURVE_RESOLUTION = 10 ;

    // Variables
    [SerializeField] protected TransformPoint[] points ;
    [SerializeField] private float length ;

    public TransformPoint[] Points { get { return points.Clone() as TransformPoint[] ; } }
    public TransformPoint Begin    { get { return points[0]         ; } }
    public TransformPoint End      { get { return points[Count - 1] ; } }

    public float Length            { get { return length ; } }
    public int Count               { get { return points.Length ; } }



    public CurveBehaviour (Vector3 start, Vector3 end) : this (
      new TransformPoint (start, Quaternion.identity),
      new TransformPoint (  end, Quaternion.identity)
    ) {}

    public CurveBehaviour (TransformPoint start, TransformPoint end) {
      points = CreatePoints(start, end) ;
      UpdateLength() ;
    }

    private CurveBehaviour () {}

    public void SetPoint (int i, TransformPoint p) {
      points[i] = p ;
      UpdateLength () ;
    }

    public Vector3 GetNormalAt (float t, Vector3 up) {
      Vector3 tangent = GetTangentAt(t),
              binormal = Vector3.Cross(up, tangent).normalized ;
  		return Vector3.Cross(tangent, binormal) ;
    }

    public Vector3 GetTangentAt (float t) {
      return GetDerivativeAt(t).normalized;
    }

    protected void UpdateLength () {
      Vector3 prev = points[0].position ;
      float newLength = 0f ;

      for (int cnt = 1; cnt <= CURVE_RESOLUTION; cnt++) {
        Vector3 current = GetPointAt(LENGTH_STEP * cnt).position ;
        newLength += Vector3.Distance(current, prev) ;
        prev = current ;
      }

      length = newLength ;
    }

    // /////////// //
    //  Abstracts
    // /////////// //
    protected abstract TransformPoint[] CreatePoints (TransformPoint start, TransformPoint end) ;

    public abstract TransformPoint GetPointAt  (float t) ;
    public abstract Vector3 GetDerivativeAt    (float t) ;


    // /////////// //
    //  Statics
    // /////////// //
    public static explicit operator TransformPoint[](CurveBehaviour c) {
      return c.points.Clone() as TransformPoint[] ;
    }

  }
}
