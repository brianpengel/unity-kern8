using System.Collections.Generic ;
using UnityEngine ;

namespace FlowEngine.Curves {

  public interface ICurve {

    TransformPoint[] Points { get; }
    TransformPoint Begin    { get; }
    TransformPoint End      { get; }
    float Length            { get; }
    int Count               { get; }


    TransformPoint GetPointAt (float t) ;
    void SetPoint             (int i, TransformPoint p) ;

    Vector3 GetNormalAt       (float t, Vector3 up) ;
    Vector3 GetTangentAt      (float t) ;
    Vector3 GetDerivativeAt   (float t) ;

  }
}
