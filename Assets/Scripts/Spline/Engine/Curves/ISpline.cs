using System.Collections.Generic ;
using UnityEngine ;

namespace FlowEngine.Curves {

  public interface ISpline<T> : ICollection<T>, ICurve where T : ICurve {

    // void AddPoint (Vector3 p) ;
    // void RemoveAt (int i) ;

  }
}
