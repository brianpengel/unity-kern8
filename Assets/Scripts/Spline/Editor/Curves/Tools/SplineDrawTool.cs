using UnityEditor ;
using UnityEngine ;
using FlowEngine.Curves ;

namespace FlowEditor.Curves {

  public class SplineDrawTool : EditorTool <Spline> {

    // public const float HANDLE_LINE_SIZE = 1f ;
    // public const float HANDLE_SNAP = .01f ;
    public const float HANDLE_SIZE = .05f ;

    private enum AxisLimiter { X, Y, Z } ;
    private AxisLimiter axisLimit = AxisLimiter.Y;
    private float planeOffset = 0f ;
    private float drawSnap = 0;

    private string assetPath ;
    private Spline spline ;


    void OnEnable () {
      if(target == null) return ;
      spline = (Spline) target ;
      assetPath = AssetDatabase.GetAssetPath(spline.Asset) ;

      inactiveIcon = "ClothInspector.PaintTool" ;
      activeIcon = "" ;
      toolTip = "Draw points" ;
    }


    public override void OnInspectorGUI () {
      var prevAxisLimit = axisLimit ;
      axisLimit = (AxisLimiter)EditorGUILayout.EnumPopup(new GUIContent("Limit drawing to plane: "), axisLimit);

      if(prevAxisLimit != axisLimit)
        planeOffset = 0;

      planeOffset = EditorGUILayout.FloatField(new GUIContent("Offset from plane:"), planeOffset);
      var fField  = EditorGUILayout.FloatField(new GUIContent("Snap points to:")   , drawSnap   ) ;
      drawSnap = fField < 0 ? 0 : fField ;
    }


    public override void DrawSceneTool () {
      PlaneDrawingToolSetup () ;
    }

    private void AddCurveToSpline (System.Type curveType, Vector3 start, Vector3 end) {
      var curve = (CurveBehaviour) System.Activator.CreateInstance (curveType,
        spline.transform.InverseTransformPoint(start),
        spline.transform.InverseTransformPoint(end)
      ) ;

      Undo.RecordObject(spline.Asset, "Added new curve (" + curveType.ToString()  + ")");
      spline.Asset.Add(curve) ;
      EditorUtility.SetDirty(spline.Asset) ;
      AssetDatabase.ImportAsset(assetPath) ;
    }

    // //////////////////// //
    //  Plane drawing mode
    // //////////////////// //
    private void PlaneDrawingToolSetup () {
      // Get camera data
      var mousePos = new Vector2(Event.current.mousePosition.x, Screen.height - Event.current.mousePosition.y - 36);
      Ray ray = SceneView.lastActiveSceneView.camera.ScreenPointToRay(mousePos);
      var normal = GetLimitedPlaneNormal(axisLimit) ;

      // Get starting point (last point in spline)
      var endPoint = spline.Count == 0 ? spline.transform.position : spline.End.position ;
      var planePosition = endPoint + normal * planeOffset ;

      // Prepare plane to draw on
      Plane hPlane = new Plane(normal, planePosition);
      float distance;

      if (!hPlane.Raycast(ray, out distance)) return ;
      var drawPosition = ray.GetPoint(distance) ;

      PlaneDrawingTool (endPoint, drawPosition, normal) ;
    }

    private void PlaneDrawingTool (Vector3 start, Vector3 end, Vector3 normal) {
      int controlID = GUIUtility.GetControlID(FocusType.Passive);

      switch (Event.current.GetTypeForControl(controlID)) {
        case EventType.Layout:
          HandleUtility.AddControl(controlID, HandleUtility.DistanceToCircle(end, HANDLE_SIZE));
          break ;

        case EventType.Repaint:
          var q = Quaternion.FromToRotation(Vector3.forward, normal) ;
          Handles.RectangleHandleCap(-1, end, q, HANDLE_SIZE, EventType.Repaint);
          Handles.DrawDottedLine(start, end, HANDLE_SIZE) ;
          break;

        case EventType.MouseDown:
          if (HandleUtility.nearestControl != controlID || Event.current.button != 0) break ;
            GUIUtility.hotControl = controlID;
            Event.current.Use();
          break;

        case EventType.MouseUp:
          if (GUIUtility.hotControl != controlID || Event.current.button != 0) break ;
          AddCurveToSpline(typeof(CubicBezierCurve), start, end) ;
          GUIUtility.hotControl = 0;
          Event.current.Use();
          break;

        case EventType.MouseMove:
          if(GUIUtility.hotControl == 0)
            SceneView.RepaintAll();
          break;
      }
    }

    private Vector3 GetLimitedPlaneNormal (AxisLimiter limit) {
      if(limit == AxisLimiter.Z)
        return Vector3.forward;
      if(limit == AxisLimiter.X)
        return Vector3.right ;
      return Vector3.up ;
    }
    
  }
}
