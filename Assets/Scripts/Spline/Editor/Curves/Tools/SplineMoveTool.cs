using UnityEditor ;
using UnityEngine ;
using FlowEngine.Curves ;
using FlowEngine ;

namespace FlowEditor.Curves {

  public class SplineMoveTool : EditorTool <Spline> {

    private enum MoveToolType { Free, Normal } ;

    // private static readonly Color HANDLE_COLOUR = Color.white ;
    private const float HANDLE_SIZE = .05f ;

    private MoveToolType curMoveToolType = MoveToolType.Normal ;
    private int selectedMoveHandle = -1;

    void OnEnable () {
      inactiveIcon = "MoveTool" ;
      activeIcon = "MoveTool On" ;
      toolTip = "Move points" ;
    }

    public override void OnInspectorGUI () {
      var oldType = curMoveToolType ;
      curMoveToolType = (MoveToolType)EditorGUILayout.EnumPopup(new GUIContent("Move method: "), curMoveToolType);

      if(oldType != curMoveToolType) {
        selectedMoveHandle = -1 ;
        SceneView.RepaintAll() ;
      }
    }

    public override void DrawSceneTool () {
      switch (curMoveToolType) {
        case MoveToolType.Free :
          DrawMoveTool ((Spline) target, DrawFreeMoveHandle) ;
          break ;

        default :
          DrawMoveTool ((Spline) target, DrawNormalMoveHandle) ;
          break ;
      }
    }

    private void DrawMoveTool (Spline spline, System.Action<Spline, int, TransformPoint> deleg) {
      var points = spline.Points ;
      for(int i = 0 ; i < points.Length ; ++ i)
        deleg(spline, i, points[i]) ;
    }

    private void DrawFreeMoveHandle (Spline s, int i, TransformPoint p) {
      EditorGUI.BeginChangeCheck();
      Vector3 newPoint = Handles.FreeMoveHandle(p.position, Quaternion.identity, HANDLE_SIZE, Vector3.zero, Handles.RectangleHandleCap) ;

      if(EditorGUI.EndChangeCheck()) {
        Undo.RecordObject(s.Asset, "Changed point position");
        p.position = newPoint ;
        s.SetPoint(i, p) ;
      }
    }

    private void DrawNormalMoveHandle (Spline s, int i, TransformPoint p) {
      if(selectedMoveHandle != i) {
        if(Handles.Button(p.position, Quaternion.identity, HANDLE_SIZE, HANDLE_SIZE, Handles.CubeHandleCap))
          selectedMoveHandle = i ;
        return ;
      }

      EditorGUI.BeginChangeCheck();
      Vector3 newPoint = Handles.PositionHandle(p.position, Quaternion.identity) ;

      if(EditorGUI.EndChangeCheck()) {
        Undo.RecordObject(s.Asset, "Changed point position");
        p.position = newPoint ;
        s.SetPoint(i, p) ;
      }
    }

  }
}
