using UnityEditor ;
using UnityEngine ;
using FlowEngine.Curves ;

namespace FlowEditor.Curves {

  public class SplineSettingsTool : EditorTool <Spline> {

    private SerializedProperty visibility ;
    private SerializedProperty colour ;

    void OnEnable () {
      inactiveIcon = "MoveTool" ;
      activeIcon = "MoveTool On" ;
      toolTip = "Move points" ;

      if(target == null) return ;
      
      visibility = serializedObject.FindProperty("isVisible") ;
      colour = serializedObject.FindProperty("renderColour") ;
    }

    public override void OnInspectorGUI () {
      serializedObject.Update () ;

      EditorGUILayout.PropertyField(visibility) ;
      EditorGUILayout.PropertyField(colour) ;

      serializedObject.ApplyModifiedProperties () ;
    }

    public override void DrawSceneTool () {

    }

  }
}
