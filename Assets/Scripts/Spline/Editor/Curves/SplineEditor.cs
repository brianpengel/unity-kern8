using UnityEditor ;
using UnityEngine ;

using FlowEngine.Curves ;

namespace FlowEditor.Curves {

  [CustomEditor(typeof(Spline), true)]
  internal class SplineEditor : Editor {

    private static readonly Color SELECTED_SPLINE_BORDER_COLOUR = Color.gray ;
    private static readonly Color SELECTED_SPLINE_COLOUR = Color.white ;
    private const float SPLINE_RENDER_RESOLUTION = .01f ;
    private const float SPLINE_RENDER_WIDTH = 2f ;

    private SplineToolGroup tools ;
    private Editor proxyInspector ;
    private Spline targetSpline ;


    private void OnEnable () {
      if(target == null) return ;
      targetSpline = (Spline) target ;
      proxyInspector = Editor.CreateEditor(targetSpline, typeof(AssetProxyEditor)) ;
      tools = Editor.CreateEditor(targetSpline, typeof(SplineToolGroup)) as SplineToolGroup ;
    }

    public override void OnInspectorGUI() {
      proxyInspector.OnInspectorGUI () ;
      if(targetSpline.Asset != null)
        tools.OnInspectorGUI () ;
    }

    public void OnSceneGUI() {
      if(targetSpline.Asset != null)
        tools.OnSceneGUI () ;
    }

    /* DrawSplineGizmos
     * Spline spline, GizmoType type
     *
     * Desc.
     * Callback for the default spline gizmos.
     *
     */
    [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
    static void DrawSplineGizmos (Spline spline, GizmoType type) {
      if(spline.Asset == null) return ;

      // Store original colours
      bool isSelected = (type & GizmoType.Selected) != 0 ;
      Color gColour = Gizmos.color ;

      // Draw Spline
      if(isSelected)
        DrawSelectedSpline (spline) ;

      else if(spline.IsVisible)
        DrawNonSelectedSpline (spline) ;

      // Reset colours
      Gizmos.color = gColour ;
    }

    private static void DrawSelectedSpline (Spline spline) {
      Gizmos.color = SELECTED_SPLINE_BORDER_COLOUR ;
      DrawSplineBorder (spline) ;

      Gizmos.color = SELECTED_SPLINE_COLOUR ;
      DrawSpline (spline) ;
    }

    private static void DrawNonSelectedSpline (Spline spline) {
      Gizmos.color = spline.RenderColour ;
      DrawSpline (spline) ;
    }

    /* DrawSplineBorder
     * Spline spline
     *
     * Desc.
     * Connects all points in (Spline) spline.
     *
     */
    public static void DrawSplineBorder (Spline spline) {
      foreach(var curve in spline.Asset) {
        var points = curve.Points ;

        for(int cnt = 1 ; cnt < points.Length ; cnt ++) {
          var prev = points[cnt - 1].ApplyTransformation(spline.transform).position ;
          var curr = points[cnt    ].ApplyTransformation(spline.transform).position ;
          Gizmos.DrawLine(prev, curr) ;
        }
      }
    }

    /* DrawSpline
     * Spline spline
     *
     * Desc.
     * Draws (Spline) spline in the scene view per curve.
     *
     */
    public static void DrawSpline (Spline spline) {
      foreach(var curve in spline.Asset) {
        var prev = curve.Begin.ApplyTransformation(spline.transform).position ;

        for(float t = SPLINE_RENDER_RESOLUTION ; t <= 1f ; t += SPLINE_RENDER_RESOLUTION) {
          var curr = curve.GetPointAt(t).ApplyTransformation(spline.transform).position ;
          Gizmos.DrawLine(prev, curr) ;
          prev = curr ;
        }
      }
    }


    // ///////////////////////////// //
    //  SplineTool group definition
    // ///////////////////////////// //
    private class SplineToolGroup : EditorToolGroup <Spline> {}
  }
}
