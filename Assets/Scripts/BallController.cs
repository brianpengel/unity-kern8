﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class BallController : NetworkBehaviour {

	[SerializeField] private float angle ;
	[SerializeField] private float radius = 0.165f ;
	[SerializeField] private float speed = 5f ;
	[SerializeField] private string killTag = "Kill" ;
	
	// Use this for initialization
	IEnumerator Start () {
		while(true) {
			// yield return new WaitForSeconds(.5f) ;
			yield return DoUpdate () ;
		}
	}

	public void Reset () {

	}

	public IEnumerator DoUpdate () {
		var a = Mathf.Deg2Rad * angle ;
		var direction = new Vector3 ( Mathf.Cos(a), Mathf.Sin(a), 0) ;

		var hit = Physics2D.CircleCast(transform.position, radius, direction);
		var collisionPoint = ((Vector3)hit.point) ;
		collisionPoint -= (collisionPoint - transform.position).normalized * radius ;

		var targetPosition = transform.position + Vector3.Project(collisionPoint - transform.position, direction) ;
		var normal = ((Vector3)hit.normal) ;

		// Move to point
		while (transform.position != targetPosition) {
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
			yield return null ;
		}

		if(isServer)
			if(hit.collider.gameObject.tag == killTag)
				// lose
				Debug.Log("Lose") ;

			else {
				var diff = Vector3.SignedAngle(direction, normal, Vector3.forward) + 180 ;
				angle = Vector3.SignedAngle(Vector3.up, normal, Vector3.forward) + 90 + diff ;
				// nextTurn
				Debug.Log("Next Player") ;
			}

	}
}
