﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;
using FlowEngine.Curves;

public class GameManager : NetworkBehaviour {

  public static GameManager Instance ;

	[SerializeField] private Text scoreFieldP1, scoreFieldP2, scoreCount ;
  [SerializeField] private int scoreIncrease = 100, maxScore = 10000, minScore = 50 ;
  [SyncVar(hook = "SyncScorer")] private int scorer ;
  [SyncVar(hook = "SyncScoreP1")] private int scoreP1 ;
  [SyncVar(hook = "SyncScoreP2")] private int scoreP2 ;

  public int P1Score { get { return scoreP1 ; } }
  public int P2Score { get { return scoreP2 ; } }

  void Awake () {
    if(Instance == null)
      Instance = this ;

    scoreP1 = scoreP2 = maxScore ;
    scorer = minScore ;
    RenderScore () ;
  }

  [Server]
  public void SetScorer(int bounces) {
    if(isServer)
      SyncScorer(bounces * scoreIncrease) ;
  }

  private void SyncScorer (int amount) {
    scorer = Mathf.Clamp(amount, minScore, maxScore) ;
    RenderScore () ;
  }

  private void SyncScoreP1 (int amount) {
    scoreP1 = Mathf.Clamp(amount, 0, maxScore) ;
    RenderScore () ;
  }

  private  void SyncScoreP2 (int amount) {
    scoreP2 = Mathf.Clamp(amount, 0, maxScore) ;
    RenderScore () ;
  }

  private void RenderScore() {
    int a, b ;
    if(isServer) {
      a = scoreP1 ;
      b = scoreP2 ;
    }

    else {
      b = scoreP1 ;
      a = scoreP2 ;
    }

    scoreFieldP1.text = a.ToString() ;
    scoreFieldP2.text = b.ToString() ;
    scoreCount.text = scorer.ToString() ;
  }

  [Server]
  public void ApplyScorer(int id) {
    if(id == 0)
         SyncScoreP1(scoreP1 - scorer) ;
    else SyncScoreP2(scoreP2 - scorer) ;
    RenderScore() ;

    if(scoreP1 == 0 || scoreP2 == 0) {
      RpcEndGame (scoreP1, scoreP2) ;
    }
  }

  [Server]
  public void EndGame () {
    RpcEndGame (scoreP1, scoreP2) ;
  }

  [ClientRpc]
  private void RpcEndGame (int a, int b) {
    (CustomNetworkUI.singleton as CustomNetworkUI).EndGame(isServer ? scoreP1 : scoreP2) ;
    // NetworkManager.singleton.StopClient () ;
  }


}
