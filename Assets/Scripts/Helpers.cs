using System.Collections;
using System.Collections.Generic;
using FlowEngine.Curves;
using UnityEngine;

public static class Helpers {

  public static float CircularT (float t) {
		if(t < 0) t = 1f + t ;
		else if(t > 1) t = t % 1f ;
		return t ;
	}

  public static Vector2[] GetEdgePoints (this LineRenderer renderer) {
		float wireWidth = renderer.widthMultiplier ,
		 			halfWidth = wireWidth * .5f ;

		var edgePoints = new List<Vector2> ();
		var points = new Vector3[renderer.positionCount + 1] ;
		renderer.GetPositions(points) ;

		// Add extra point to collider to span entire line
		points[renderer.positionCount] = points[renderer.positionCount - 1] + points[renderer.positionCount - 1] - points[renderer.positionCount - 2] ;

		for (int j = 1 ; j < points.Length ; j++) {
      float width = renderer.widthCurve.Evaluate(1f / points.Length * j) * .25f ;
			Vector3 a = points[j - 1], b = points[j] ;
			Vector2 dir = a - b ;
	    Vector3 cross = Vector3.Cross (dir, Vector3.forward).normalized ;
	    Vector2 down = -width * cross + a ;
			Vector2 up 	 =  width * cross + a ;
	    edgePoints.Insert(0, down);
	    edgePoints.Add(up);
		}

    return edgePoints.ToArray() ; ;
	}

}
