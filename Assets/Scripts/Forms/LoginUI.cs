﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class LoginUI : Form {

	[SerializeField] private InputField mailField, passwordField ;

	void Awake () {
		base.Awake () ;
		values.Add("password", "wasd") ;
		values.Add("session_id", "") ;
		values.Add("display", "") ;
		values.Add("bDay", "") ;
		values.Add("mail", "wasd@hotmail.com") ;
		SetFields() ;
	}

	public override void CollectData () {
		values["password"] = passwordField.text ;
		values["mail"] = mailField.text ;
  }

	public override void SetFields () {
		passwordField.text = values["password"] ;
		mailField.text = values["mail"] ;
	}

	public override bool Validate (out string msg) {
		msg = "" ;

		if(System.String.IsNullOrEmpty(passwordField.text) || System.String.IsNullOrEmpty(mailField.text)) {
			msg = "All field need to be filled." ;
			return false ;
		}

    return true ;
  }

	public override void OnResponse (string res, string err) {
		var response = DefaultResponseJSON(res, err) ;
		ClearError () ;

		if(response == null)
			ThrowError(err) ;

		if(!ResponseContainsKeys(response, "session_id", "display"))
			if(response.ContainsKey("error"))
					 ThrowError(response["error"] as string) ;
			else ThrowError("Unexpected response.") ;

		else {
			values["session_id"] = response["session_id"] as string ;
			values["display"] = response["display"] as string ;
			values["bDay"] = response["bDay"] as string ;
			onResponse.Invoke () ;
		}
	}

}
