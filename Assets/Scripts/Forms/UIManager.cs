﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour {

  private const string BASE_URL = "http://studenthome.hku.nl/~brian.pengel/school/blok8/Database_KernGDV4_BrianPengel_3014495/EindOpdracht" ;

	[SerializeField] private List<Form> forms ;
  [SerializeField] private int start = 0 ;
  private Coroutine curRequest ;
  private int currentI ;

  void Awake () {
    if(forms == null)
      forms = new List<Form> () ;
    ChangeForm (0) ;
  }

  public void ChangeForm (int i) {
    foreach (var f in forms)
      if(f != null) {
        f.gameObject.SetActive(true) ;
        f.gameObject.SetActive(false) ;
      }

    forms[Mathf.Min(i, forms.Count)].gameObject.SetActive(true) ;
    currentI = i ;
  }

  public void ChangeForm (Form form) {
    int i = -1 ;
    for (int cnt = 0 ; cnt < forms.Count ; cnt ++)
      if(forms[cnt] != form) {
        if(forms[cnt] != null)
          forms[cnt].gameObject.SetActive(false) ;
      }
      else i = cnt ;

    if(i == -1) {
      currentI = forms.Count ;
      forms.Add(form) ;
      form.gameObject.SetActive(true) ;
    }

    else {
      currentI = i ;
      forms[currentI].gameObject.SetActive(true) ;
    }

    if(forms[currentI] != null)
      forms[currentI].gameObject.SetActive(true) ;
  }

  public void FillCurrent (Form vals) {
    forms[currentI].Fill(vals) ;
  }

}
