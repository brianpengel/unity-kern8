using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class RegisterUI : Form {

	[SerializeField] private InputField mailField, displayField, passwordField ;
  [SerializeField] private Dropdown dayField, monthField, yearField ;

  void Awake () {
    base.Awake () ;
    values.Add("session_id", "") ;
    values.Add("password", "") ;
		values.Add("display", "") ;
		values.Add("mail", "") ;
		values.Add("bDay", "") ;

    yearField.options = CreateOptions (150, i=>{
			var res = new Dropdown.OptionData();
			res.text = (System.DateTime.Now.Year - i).ToString() ;
			return res ;
		}) ;

		monthField.options = CreateOptions (12, null, 1) ;
		dayField.options = CreateOptions (31, null, 1) ;
  }

  public override void CollectData () {
		values["password"] = passwordField.text ;
		values["display"] = displayField.text ;
		values["mail"] = mailField.text ;

		var date = System.DateTime.Parse(
			yearField.options[yearField.value].text + " " +
			monthField.options[monthField.value].text + " " +
			dayField.options[dayField.value].text
		);

		values["bDay"] = date.ToString("yyyy-MM-dd") ;
  }

  public override void SetFields () {
		passwordField.text = values["password"] ;
		displayField.text = values["display"] ;
		mailField.text = values["mail"] ;

		if(System.String.IsNullOrEmpty(values["bDay"]))
			return ;

		var date = System.DateTime.Parse(values["bDay"]) ;
    yearField.value 	= FindOptionIndex(yearField.options, 	date.Year.ToString()) ;
    monthField.value 	= FindOptionIndex(monthField.options, date.Month.ToString()) ;
    dayField.value 		= FindOptionIndex(dayField.options, 	date.Day.ToString()) ;
	}


	public override bool Validate (out string msg) {
		msg = "" ;

		if(System.String.IsNullOrEmpty(mailField.text) || System.String.IsNullOrEmpty(displayField.text) || System.String.IsNullOrEmpty(passwordField.text)) {
			msg = "All field need to be filled." ;
			return false ;
		}

    return true ;
  }

	public override void OnResponse (string res, string err) {
		Debug.Log(res) ;
		var response = DefaultResponseJSON(res, err) ;
		ClearError () ;

		if(response == null)
			ThrowError(err) ;

		else if(!ResponseContainsKeys(response, "success", "session_id", "display", "bDay"))
			if(response.ContainsKey("error"))
					 ThrowError(response["error"] as string) ;
			else ThrowError("Unexpected response.") ;

		else {
			values["session_id"] = response["session_id"] as string ;
			values["display"] = response["display"] as string ;
			values["bDay"] = response["bDay"] as string ;
			onResponse.Invoke () ;
		}
	}




	private List<Dropdown.OptionData> CreateOptions (int count, System.Func<int, Dropdown.OptionData> deleg = null, int offset = 0) {
		deleg = deleg == null ? DefaultFactory : deleg ;
		var options = new List<Dropdown.OptionData>();

		for(int cnt = offset; cnt < count + offset; ++ cnt)
			options.Add(deleg(cnt));

		return options ;
	}

	private Dropdown.OptionData DefaultFactory (int i) {
		var res = new Dropdown.OptionData();
		res.text = i.ToString() ;
		return res ;
	}

	private int FindOptionIndex (List<Dropdown.OptionData> opts, string target) {
		for (int cnt = 0; cnt < opts.Count; cnt ++)
      if(opts[cnt].text == target)
				return cnt ;
		return 0 ;
	}
}
