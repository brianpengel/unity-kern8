using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EditUI : RegisterUI {

	public override void OnResponse (string res, string err) {
		var response = DefaultResponseJSON(res, err) ;
		ClearError () ;

		if(response == null)
			ThrowError(err) ;

		else if(!ResponseContainsKeys(response, "success", "display", "password", "bDay", "mail"))
			if(response.ContainsKey("error"))
					 ThrowError(response["error"] as string) ;
			else ThrowError("Unexpected response.") ;

		else {
			values["display"] = response["display"] as string ;
			values["password"] = response["password"] as string ;
			values["bDay"] = response["bDay"] as string ;
			values["mail"] = response["mail"] as string ;
			onResponse.Invoke () ;
      SetFields () ;
		}
	}

}
