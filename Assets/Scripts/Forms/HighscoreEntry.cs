using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HighscoreEntry : MonoBehaviour {

  [SerializeField] private Text labelField, scoreField ;

  public void Init(string label, string score) {
    if(labelField != null) labelField.text = label ;
    if(scoreField != null) scoreField.text = score ;
  }

}
