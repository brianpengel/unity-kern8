using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MenuUI : Form {

  [SerializeField] private Text userNameField ;

  void Awake () {
    base.Awake () ;
    values.Add("session_id", "") ;
    values.Add("password", "") ;
		values.Add("display", "") ;
		values.Add("mail", "") ;
		values.Add("bDay", "") ;
  }

  public override void CollectData () {

  }

  public override void SetFields () {
    if(userNameField != null)
      userNameField.text = values["display"] ;
	}

}
