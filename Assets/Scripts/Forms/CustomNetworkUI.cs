using System.Collections.Generic ;
using System.Collections ;
using UnityEngine.Networking ;
using UnityEngine.UI ;
using UnityEngine ;

using FlowEngine.Curves;

public class CustomNetworkUI : NetworkManager {

  [SerializeField] private Form SubmitScoreForm ;
  [SerializeField] private Text ipField ;

  private bool CanStart { get { return !NetworkClient.active && !NetworkServer.active && NetworkManager.singleton.matchMaker == null ; } }
  private bool IsActive { get { return NetworkServer.active && NetworkClient.active ; } }

  void Start () {
    var buttons = GameObject.FindGameObjectsWithTag("Button Proxy")[0].GetComponent<ButtonProxy> () ;
    buttons["Host"] = StartHost ;
    buttons["Join"] = JoinGame ;
  }

  public void StartHost () {
    if(!CanStart) Close() ;
    SetPort() ;
    NetworkManager.singleton.StartHost() ;
  }

  public void JoinGame () {
    if(!CanStart) Close() ;
    SetIPAddress () ;
    SetPort () ;
    NetworkManager.singleton.StartClient();
  }

  void SetIPAddress () {
    var ip = ipField.text ;
    NetworkManager.singleton.networkAddress = ip ;
  }

  void SetPort () {
    NetworkManager.singleton.networkPort = 7777 ;
  }

  public void Close () {
    NetworkManager.singleton.StopHost();
    NetworkServer.Reset () ;
  }

  void OnLevelWasLoaded(int level) {
    Time.timeScale = 1 ;
    if(level == 0)
         SetupMenuSceneButtons () ;
    else SetupGameSceneButtons () ;
  }

  void SetupMenuSceneButtons () {
    Close () ;
    var buttons = GameObject.FindGameObjectsWithTag("Button Proxy")[0].GetComponent<ButtonProxy> () ;
    buttons["Host"] = StartHost ;
    buttons["Join"] = JoinGame  ;
  }

  void SetupGameSceneButtons () {
    // var discBtn = GameObject.FindGameObjectsWithTag("DisconnectButton")[0].GetComponent<Button> () ;
    // discBtn.onClick.RemoveAllListeners () ;
    // discBtn.onClick.AddListener (Close) ;
  }

	public override void OnClientDisconnect(NetworkConnection conn) {
    GameManager.Instance.EndGame () ;
    base.OnClientDisconnect(conn) ;
	}

  public override void OnServerDisconnect(NetworkConnection conn) {
    GameManager.Instance.EndGame () ;
    base.OnServerDisconnect(conn) ;
	}

  public void EndGame (int score) {
    Time.timeScale = 0 ;
    if(score > 0) {
      SubmitScoreForm ["score"] = score.ToString() ;
      SubmitScoreForm.POST() ;
    }

    Debug.LogError("Score " + score) ;
    NetworkManager.singleton.StopClient () ;
  }

  public void EndGame () {
    GameManager.Instance.EndGame () ;
    Time.timeScale = 0 ;
    Debug.LogError("Quit early") ;
    NetworkManager.singleton.StopClient () ;
  }

}
