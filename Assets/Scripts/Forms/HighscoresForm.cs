using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HighscoresForm : Form {

  [SerializeField] private string gamesURL = "games.php", scoresURL = "scores.php" ;
  [SerializeField] private Dropdown gameField, rangeField ;
  [SerializeField] private HighscoreEntry entryPrefab ;
  [SerializeField] private RectTransform scrollContent ;
  [SerializeField] private Text timesPlayed ;
  private List<object> gameData ;

  void Awake () {
    base.Awake () ;
    values.Add("game_id", "0") ;
		values.Add("interval", "0") ;
  }

  void OnEnable () {
    LoadGames () ;
    SetFields () ;
  }

  public void LoadGames () {
    StartCoroutine(GetForm(gamesURL, values, OnGamesResponse)) ;
  }

  public void LoadScores () {
    CollectData () ;
    StartCoroutine(GetForm(scoresURL, values, OnScoresResponse)) ;
  }

  public override void CollectData () {
		values["game_id"] = (gameField.value + 1).ToString() ;
		values["interval"] = (rangeField.value).ToString() ;
  }

  public override void SetFields () {
		gameField.value = System.Convert.ToInt32(values["game_id"]) - 1 ;
		rangeField.value = System.Convert.ToInt32(values["interval"]) ;

    if(gameData == null)
      return ;
    var p = gameData[gameField.value] as Dictionary<string, object> ;
    timesPlayed.text = "Played " + (p["count"] as string) + " times" ;
	}

  public void OnGamesResponse (string res, string err) {
		var response = DefaultResponseJSON(res, err) ;
		ClearError () ;

		if(response == null)
			ThrowError(err) ;

		else if(!ResponseContainsKeys(response, "success", "games"))
			if(response.ContainsKey("error"))
					 ThrowError(response["error"] as string) ;
			else ThrowError("Unexpected response.") ;

		else {
      var options = new List<Dropdown.OptionData>();
      gameData = response["games"] as List<object> ;

			foreach(Dictionary<string, object> p in gameData) {
        var opt = new Dropdown.OptionData();
    		opt.text = p["name"] as string ;
  			options.Add(opt);
      }

      gameField.options = options ;
      LoadScores () ;
			onResponse.Invoke () ;
		}
	}

  public void OnScoresResponse (string res, string err) {
    Debug.Log(res) ;
		var response = DefaultResponseJSON(res, err) ;
		ClearError () ;

		if(response == null)
			ThrowError(err) ;

		else if(!ResponseContainsKeys(response, "success", "scores"))
			if(response.ContainsKey("error"))
					 ThrowError(response["error"] as string) ;
			else ThrowError("Unexpected response.") ;

		else {
      var scores = response["scores"] as List<object> ;

       foreach (RectTransform child in scrollContent.transform)
         Destroy(child.gameObject) ;

			for(int cnt = 0 ; cnt < scores.Count ; cnt ++) {
        var score = scores[cnt] as Dictionary<string, object> ;
        var entry = Instantiate(entryPrefab, Vector3.zero, Quaternion.identity) as HighscoreEntry ;
        entry.transform.SetParent(scrollContent, false) ;
        entry.transform.localPosition = entry.transform.localPosition + -Vector3.up * 32 * cnt ;
        entry.Init(score["display_name"] as string, score["score"] as string) ;
      }

			onResponse.Invoke () ;
		}
	}

}
