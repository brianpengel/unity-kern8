using System.Collections.Generic;
using System.Collections;
using System;

using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;
using FlowEngine;
using MiniJSON ;


public abstract class Form : MonoBehaviour {

	private const string BASE_URL = "http://studenthome.hku.nl/~brian.pengel/school/blok8/Database_KernGDV4_BrianPengel_3014495/EindOpdracht" ;

	public enum FormState { INACTIVE, ACTIVE }

  [SerializeField] protected ScriptedEvent onStateChange ;
	[SerializeField] protected UnityEvent onResponse ;
	[SerializeField] private Text errorField ;
	[SerializeField] private string url ;

	protected Dictionary<string, string> values ;
	private Selectable[] elements ;

	public string this[string i] {
		get { return values[i] ; }
		set {
			if(!values.ContainsKey(i))
				values.Add(i, value) ;
			values[i] = value ;
		}
	}


	public FormState State {
		get ; private set ;
	}

	public string targetURL {
    get { return url ; }
    set { url = value ; }
  }

	//
	public abstract void CollectData () ;
	public abstract void SetFields () ;


	protected void Awake () {
		onStateChange = onStateChange == null ? ScriptableObject.CreateInstance <ScriptedEvent> () : onStateChange ;
		elements = GetComponentsInChildren <Selectable> (true) ;
  	values = new Dictionary<string, string> () ;
		State = FormState.ACTIVE ;
	}

	public void Fill (Dictionary<string, string> data) {
    if(data != null) {
      values = data ;
			SetFields () ;
		}
  }

	public void Fill (Form b) {
    var aVals = GetData () ;
    var bVals = b.GetData () ;

    if(aVals == null || bVals == null)
      return ;

    foreach (var d in bVals)
      if(aVals.ContainsKey(d.Key))
        aVals[d.Key] = d.Value ;

    Fill(aVals) ;
  }

	public Dictionary<string, string> GetData () {
		CollectData () ;
    return values ;
  }

	public void ChangeState (FormState state) {
		if(State != state) {
			State = state ;
			onStateChange.Raise<FormState> (state) ;

			bool active = state == FormState.ACTIVE ;
			foreach(var e in elements)
				e.interactable = active ;
		}
	}

	// Error
	public virtual bool Validate (out string msg) {
		msg = "" ;
    return true ;
  }

	protected virtual void ThrowError(string err) {
		if(errorField != null)
			errorField.text = err ;
		Debug.LogError("ERROR  | " + (err == null ? "NULL" : err)) ;
	}

	protected virtual void ClearError() {
		if(errorField != null)
			errorField.text = "" ;
	}


	/// Default POST/GET
	// POST
  public void POST () {
		string msg ;
		if(Validate(out msg))
    	StartCoroutine(PostForm(url, this.values, OnResponse)) ;
		else ThrowError(msg) ;
  }

	// GET
  public void GET () {
		string msg ;
		if(Validate(out msg))
    	StartCoroutine(GetForm(url, this.values, OnResponse)) ;
		else ThrowError(msg) ;
  }

	public virtual void OnResponse (string res, string err) {
		Debug.Log("RESULT | " + res) ;
		Debug.Log("ERROR  | " + err) ;
		onResponse.Invoke() ;
	}

  protected IEnumerator PostForm(string targetURL, Dictionary<string, string> values, System.Action<string, string> deleg) {
		CollectData() ;
		ChangeState(FormState.INACTIVE) ;

    var url = System.IO.Path.Combine(BASE_URL, targetURL) ;
    var www = new WWWForm();

    foreach(var val in values)
      www.AddField( val.Key, val.Value ) ;

    var req = new WWW(url, www);
    yield return req;

    ChangeState(FormState.ACTIVE) ;
		deleg (req.text, req.error) ;
  }

  protected IEnumerator GetForm(string targetURL, Dictionary<string, string> values, System.Action<string, string> deleg) {
		CollectData() ;
		ChangeState(FormState.INACTIVE) ;

    var url = System.IO.Path.Combine(BASE_URL, targetURL) ;

		if(values.Count > 0) {
			url += "?" ;
			foreach(var val in values)
	    	url += val.Key + "=" + val.Value + "&" ;
			url = url.Remove(url.Length - 1);
		}

    var req = new WWW(url);
    yield return req;

    ChangeState(FormState.ACTIVE) ;
		deleg (req.text, req.error) ;
  }



	public static Dictionary<string, object> DefaultResponseJSON (string res, string err) {
		if(!System.String.IsNullOrEmpty(err))
			return null ;

		return Json.Deserialize(res) as Dictionary<string, object> ;
	}

	public static bool ResponseContainsKeys (Dictionary<string, object> res, params string[] requiredKeys) {
		if(res == null) return false ;
		foreach(var key in requiredKeys)
			if(!res.ContainsKey(key))
				return false ;
		return true ;
	}

}
