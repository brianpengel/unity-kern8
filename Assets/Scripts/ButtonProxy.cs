﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine;
using FlowEngine.Curves;

public class ButtonProxy : MonoBehaviour {

  private Dictionary<string, System.Action> buttons ;

	public System.Action this[string i] {
    get {
      if(buttons == null)
        buttons = new Dictionary<string, System.Action> () ;
      return buttons[i] ;
    }
    set {
      if(buttons == null)
        buttons = new Dictionary<string, System.Action> () ;
        
      if(buttons.ContainsKey(i))
        buttons[i] = value ;
      else buttons.Add(i, value) ;
    }
  }

  public void DoButton(string name) {
    buttons[name] () ;
  }

}
