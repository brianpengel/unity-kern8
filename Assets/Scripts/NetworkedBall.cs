﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using FlowEngine.Curves;

public class NetworkedBall : TurnActor {

  [SerializeField] private BounceImpact impact ;

	[SerializeField] private string killTag = "Kill" ;
  [SerializeField] private float radius = 0.165f ;
  [SerializeField] private float speed = 5f ;
  [SerializeField] private float angle ;
  private GameManager gameManager ;
  private int bounces = 0 ;

  private Coroutine remoteMoveRoutine ;
  private bool didDie = true ;
  private Vector3 spawn ;

  void Awake() {
    spawn = transform.position ;
  }

  public void Inject(GameManager gm) {
    gameManager = gm ;
  }

	protected override IEnumerator OnTurn () {
    if(!isServer) yield break ;
    yield return LocalTurn () ;
  }

  private IEnumerator LocalTurn () {
    var direction = AngleTorDirection(angle) ;
		var hit = WillCollide(direction) ;
		var collisionPoint = PointOfCollision(hit) ;
    var targetPosition = GetTargetPosition(collisionPoint, direction) ;

    CmdForceMove (targetPosition, false) ;
    yield return MoveToCollisionPoint (targetPosition) ;
    CmdForceCollision (targetPosition) ;

    // On collision with boundary
		if(hit.collider.gameObject.tag == killTag)
      Die () ;
    // On collision with others
		else {
      CmdForceMove (targetPosition, true) ;

      ++ bounces ;
      var col = hit.collider ;
      var normal = ((Vector3)hit.normal) ;
			var diff = Vector3.SignedAngle(direction, normal, Vector3.forward) + 180 ;
			angle = Vector3.SignedAngle(Vector3.up, normal, Vector3.forward) + 90 + diff ;
      gameManager.SetScorer(bounces) ;
      didDie = false ;
      EndTurn () ;
		}


  }

  private void Die () {
    bounces = 0 ;
    transform.position = spawn ;
    CmdForceMove (spawn, true) ;
    gameManager.ApplyScorer(TurnManager.Instance.CurrentActorID) ;
    gameManager.SetScorer(bounces) ;
    didDie = true ;
    EndTurn () ;
  }


  private Vector3 GetTargetPosition (Vector3 point, Vector3 dir) {
    // cxalculate endpoint
		return transform.position + Vector3.Project(point - transform.position, dir) ;
  }

  private IEnumerator MoveToCollisionPoint(Vector3 point) {
		while (transform.position != point) {
			transform.position = Vector3.MoveTowards(transform.position, point, speed * Time.deltaTime);
			yield return null ;
		}

    // impact.Bounce(transform.position) ;
  }

  private IEnumerator RemoteMoveToCollisionPoint(Vector3 point) {
    var move = MoveToCollisionPoint (point) ;

    while(activeActor)
	    yield return move.MoveNext() ;
    transform.position = point ;
  }

  private Vector3 AngleTorDirection (float angle) {
    var a = Mathf.Deg2Rad * angle ;
		return new Vector3 ( Mathf.Cos(a), Mathf.Sin(a), 0) ;
  }

  private RaycastHit2D WillCollide (Vector3 dir) {
    return Physics2D.CircleCast(transform.position, radius, dir) ;
  }

  private Vector3 PointOfCollision (RaycastHit2D hit) {
    var collisionPoint = ((Vector3)hit.point) ;
		return collisionPoint - (collisionPoint - transform.position).normalized * radius ;
  }


  // Remote prediction
  [Command]
	void CmdForceMove(Vector3 pos, bool instant){
    RpcForceMove (pos, instant) ;
	}

  [ClientRpc]
	void RpcForceMove(Vector3 pos, bool instant){
    if(isLocalPlayer) return ;

    if(remoteMoveRoutine != null)
      StopCoroutine(remoteMoveRoutine) ;

    if(instant)
       transform.position = pos ;

	  else remoteMoveRoutine = StartCoroutine(MoveToCollisionPoint( pos )) ;
	}

  //
  [Command]
	void CmdForceCollision(Vector3 pos){
    RpcForceCollision (pos) ;
	}

  [ClientRpc]
	void RpcForceCollision(Vector3 pos){
    impact.Bounce(pos) ;
	}



  protected override void OnTurnStart () {
    // Debug.Log("Ball turn start - local player: " + isLocalPlayer) ;
  }

  protected override void OnTurnEnd () {

    // Debug.Log("Ball end - local player: " + isLocalPlayer) ;
    // if(remoteMoveRoutine != null)
    //   StopCoroutine(remoteMoveRoutine) ;
  }


}
