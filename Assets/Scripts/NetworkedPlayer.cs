﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using FlowEngine.Curves;

public class NetworkedPlayer : TurnActor {

  [SerializeField, SyncVar (hook="OnSetPosition"), Range(0f, 1f)] float t ;
  [SerializeField, SyncVar] private float width = 5 ;
  [SerializeField, SyncVar] private float speed = 5 ;
  [SerializeField, SyncVar] private float beginT = 0 ;
  [SerializeField, SyncVar] private float endT = 1 ;
  [SerializeField, SyncVar] private int actorID ;

  [SerializeField, Range(0f, .5f)] private float shadowAlpha = .3f ;
  [SerializeField] private LineRenderer shadow ;
  [SerializeField] private float resolution = 5 ;
	[SerializeField] private Color local = Color.blue, notLocal = Color.red, inactive = Color.gray ;
	[SerializeField] private Spline path ;

  new private PolygonCollider2D collider ;
	private LineRenderer renderer ;
  private Color colour, lerpedColour ;
	private Mesh mesh ;
  private float tDifference ;

  private float SpeedMultiplier { get { return Time.deltaTime * (speed / path.Length) ; } }



  void Awake () {
		collider = GetComponent<PolygonCollider2D> () ;
		renderer = GetComponent<LineRenderer> () ;

    gameObject.name = "Remote Player" ;
    colour = inactive ; // notLocal ;
    lerpedColour = Color.Lerp(colour, inactive, .5f) ;
    SetColour(renderer, lerpedColour) ;
	}

  public override void OnStartLocalPlayer() {
		gameObject.name = "Local Player" ;
    colour = local ;
    lerpedColour = Color.Lerp(colour, inactive, .5f) ;
    SetColour(renderer, lerpedColour) ;

    UpdateVisuals(t) ;
  }

  public override void StartGame () {
    base.StartGame () ;
    actorID = TurnManager.Instance.GetActorID(this) ;
    CmdSetPosition (actorID == 0 ? 0 : .5f) ;
    CmdForceRender () ;
    UpdateCollider () ;
  }

	protected override IEnumerator OnTurn () {
    var turn = isLocalPlayer ? LocalTurn () : RemoteTurn () ;
    yield return turn ;
  }

  private IEnumerator LocalTurn () {
    while(!Input.GetKeyUp(KeyCode.Return)) {
      yield return null ;
      var input = Input.GetAxisRaw("Vertical") ;
  		t = Helpers.CircularT ( t + input * SpeedMultiplier ) ;

      CmdSetPosition (t) ;
      UpdateVisuals (t) ;
      CmdSetInput (input) ;
    }

    EndTurn () ;
  }

  // Prediction test
  private IEnumerator RemoteTurn () {
    while(activeActor) {
      yield return null ;
  		t = Helpers.CircularT ( t + tDifference * SpeedMultiplier ) ;
      UpdateVisuals (t) ;
    }
  }

  protected override void OnTurnStart () {
    UpdateShadow () ;
    SetColour(renderer, colour) ;
    UpdateVisuals (t) ;
  }

  protected override void OnTurnEnd () {
    UpdateCollider () ;
    SetColour(renderer, lerpedColour) ;
    UpdateShadow () ;
    tDifference = 0 ;
  }


  // Getters / Setters
  void OnSetPosition(float t) {
		if(!isLocalPlayer) {
			this.t = t ;
      UpdateVisuals (t) ;
    }
	}

	[Command]
	void CmdSetPosition(float t){
		this.t = t ;
	}

  // used for prediction
  [Command]
	void CmdSetInput(float t){
		// tDifference = t ;
    RpcSetInput (t) ;
	}

  [ClientRpc]
	void RpcSetInput(float t){
	  tDifference = t ;
	}

  // force render remote
  [Command]
	void CmdForceRender(){
    RpcForceRender () ;
	}

  [ClientRpc]
	void RpcForceRender(){
    UpdateShadow () ;
	  UpdateVisuals(t) ;
	}


  // Updates
  private void UpdateVisuals (float t) {
		int steps = Mathf.CeilToInt(width * resolution);
		var positions = new Vector3[steps] ;

		float length = 1f / path.Length * width,
					step = length / steps,
					halfL = length * .5f ;

		// Used for collision checking
		beginT = t - halfL ;
		endT   = t + halfL ;


		for(int cnt = 0 ; cnt < steps ; ++ cnt) {
			float posT = Helpers.CircularT(beginT + step * cnt) ;
			positions[cnt] = path.GetPointAt(posT).position ;
		}

		renderer.positionCount = steps ;

    AnimationCurve curve = new AnimationCurve();
    curve.AddKey(0.0f, 0f);
    curve.AddKey(0.5f, 1f);
    curve.AddKey(1.0f, 0f);

    renderer.widthCurve = curve;
		renderer.SetPositions(positions) ;
	}

	private void UpdateCollider () {
    UpdateVisuals (t) ;

    if(isServer)
		  collider.points = renderer.GetEdgePoints() ;
	}

  private void SetColour (LineRenderer renderer, Color colour, float alpha = 1) {
		Gradient gradient = new Gradient();

    gradient.SetKeys(
      new GradientColorKey[] { new GradientColorKey(colour, 0.0f), new GradientColorKey(colour, 1.0f) },
      new GradientAlphaKey[] { new GradientAlphaKey(alpha,  0.0f), new GradientAlphaKey(alpha,  1.0f) }
    );

    renderer.colorGradient = gradient;
	}

  // The point ?
  private void UpdateShadow () {
    // shadow.widthCurve = renderer.widthCurve ;
    // Vector3[] points = new Vector3[Mathf.CeilToInt(width * resolution)] ;
    // shadow.positionCount = points.Length ;
    // Debug.Log(renderer.GetPositions(points)) ;
		// shadow.SetPositions(points) ;
    // SetColour(shadow, colour, shadowAlpha) ;
  }


}
