﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using FlowEngine.Curves;
using UnityEngine;

[RequireComponent(typeof(PolygonCollider2D)), RequireComponent(typeof(LineRenderer))]
public class PlayerController : NetworkBehaviour  {


	[SerializeField] private float resolution = 5 ;
	[SerializeField, SyncVar] private float width = 5 ;
	[SerializeField, SyncVar] private float speed = 5 ;
	[SerializeField, SyncVar (hook="OnSetPosition"), Range(0f, 1f)] float t ;
	[SerializeField] private Color local = Color.blue, notLocal = Color.red ;
	[SerializeField] private Spline path ;

	new private PolygonCollider2D collider ;
	private LineRenderer renderer ;
	private Mesh mesh ;

	public float Begin { get; private set; }
	public float End	 { get; private set; }

	void Awake () {
		collider = GetComponent<PolygonCollider2D> () ;
		renderer = GetComponent<LineRenderer> () ;
		SetColour(notLocal) ;
		UpdateVisuals (t) ;
	}

	public override void OnStartLocalPlayer() {
		gameObject.name = "local Player" ;
		CmdSetPosition (isServer ? 0 : .5f) ;
		SetColour(local) ;
  }

	public void Init (Spline path, float t) {
		this.path = path ;
		this.t = t ;
	}

	void Update () {
		if (isLocalPlayer && path != null)
			Movement () ;
		UpdateVisuals (t) ;
	}

	private void Movement () {
		var change = Input.GetAxisRaw("Vertical") * Time.deltaTime * (speed / path.Length) ;
		t = Helpers.CircularT ( t + change ) ;
		CmdSetPosition (t) ;
	}


	private void UpdateVisuals (float t) {
		int steps = Mathf.CeilToInt(width * resolution);
		var positions = new Vector3[steps] ;

		float length = 1f / path.Length * width,
					step = length / steps,
					halfL = length * .5f ;

		// Used for collision checking
		Begin = t - halfL ;
		End 	= t + halfL ;


		for(int cnt = 0 ; cnt < steps ; ++ cnt) {
			float posT = Helpers.CircularT(Begin + step * cnt) ;
			positions[cnt] = path.GetPointAt(posT).position ;
		}

		renderer.positionCount = steps ;
		renderer.SetPositions(positions) ;
	}

	private void UpdateCollider () {
		collider.points = renderer.GetEdgePoints() ;
	}

	[Command]
	void CmdSetWidth(float w){
		this.width = w ;
	}

	void OnSetPosition(float t) {
		if(!isLocalPlayer)
			this.t = t ;
	}

	[Command]
	void CmdSetPosition(float t){
		if(!isLocalPlayer)
			this.t = t ;
	}

	private void SetColour (Color colour) {
		Gradient gradient = new Gradient();

    gradient.SetKeys(
      new GradientColorKey[] { new GradientColorKey(colour, 0.0f), new GradientColorKey(colour, 1.0f) },
      new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(1f, 1.0f) }
    );

    renderer.colorGradient = gradient;
	}
}
