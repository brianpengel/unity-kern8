﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

public class TurnManager : NetworkBehaviour {

  public static TurnManager Instance ;

  [SerializeField] private int requiredActors = 2 ;
  private List<TurnActor> actors ;
  private TurnActor ball ;
  private int currentTurn = 0 ;

  public TurnActor CurrentActor   { get { return actors[ currentTurn ] ; } }
  public int CurrentActorID       { get { return currentTurn ; } }


  void Awake () {
    if(Instance == null)
      Instance = this ;

    actors = new List<TurnActor>();
  }

  public void RegisterTurnActor (TurnActor actor) {
    // account for the ball
    if(actor is NetworkedBall)
      ball = actor ;

    else if (actors.Count < requiredActors)
      actors.Add(actor) ;

    if(actors.Count == requiredActors && ball != null)
      StartGame () ;
	}

	public void DeregisterTurnActor (TurnActor actor) {
    if(actor is NetworkedBall)
         ball = null ;
    else actors.Remove(actor);
  }

  [Server]
  public int GetActorID (TurnActor actor) {
    return actors.IndexOf(actor) ;
  }

  [Server]
  private void StartGame () {
    currentTurn = 0 ;

    foreach(var a in actors)
      a.StartGame () ;

    ball.StartGame() ;
    // ball.StartTurn () ;
    StartCurrentTurn () ;
  }

  [Server]
  private void StartCurrentTurn () {
    foreach(var a in actors)
      a.OnNewTurn () ;

    actors[ currentTurn ].StartTurn () ;
  }

  [Server]
  public void EndTurn (TurnActor actor) {
    if(actor == ball)
      NextTurn () ;

    else if(actors[ currentTurn ] == actor)
      ball.StartTurn () ;
  }

  [Server]
  private void NextTurn () {
    currentTurn = (currentTurn + 1) % actors.Count ;
    StartCurrentTurn () ;
  }

}
