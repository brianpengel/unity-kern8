﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode()]
public class ImpactShaderController : MonoBehaviour {

	[SerializeField] private Material material ;
	[SerializeField] private Vector4[] points = new Vector4[50] ;
	int cnt = 0 ;

	void Awake () {
		for(int cnt = 0 ; cnt < points.Length ; cnt ++)
			points[cnt][3] = 1 ;
	}

	void Update () {
		material.SetInt("_PointsCount", points.Length) ;
		material.SetVectorArray("_Points", points) ;

		for(int cnt = 0 ; cnt < points.Length ; cnt ++)
			points[cnt][3] += Time.deltaTime ;

		if(Input.GetMouseButtonUp(0)) {
			RaycastHit hit ;
			var mousePos = Input.mousePosition ;
			var ray = Camera.main.ScreenPointToRay(mousePos);
			if(Physics.Raycast(ray, out hit)) {
				var pos = hit.point ;
				points[cnt] = new Vector4(pos.x, pos.y, pos.z, 0) ;
				cnt = (cnt + 1) % 50 ;
			}
		}
	}
}
