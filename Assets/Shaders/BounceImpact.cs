﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceImpact : MonoBehaviour {

	[SerializeField] private Material material ;
	[SerializeField] private Vector4[] points = new Vector4[10] ;
	int cnt = 0 ;

	void Awake () {
		for(int cnt = 0 ; cnt < points.Length ; cnt ++)
			points[cnt][3] = 1 ;
	}

	void Update () {
		material.SetInt("_PointsCount", points.Length) ;
		material.SetVectorArray("_Points", points) ;

		for(int cnt = 0 ; cnt < points.Length ; cnt ++)
			points[cnt][3] += Time.deltaTime ;
	}

	public void Bounce(Vector3 pos) {
		points[cnt] = new Vector4(pos.x, pos.y, pos.z, 0) ;
		cnt = (cnt + 1) % points.Length ;
	}

}
