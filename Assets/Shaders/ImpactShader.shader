Shader "Custom/ImpactShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_ImpactSize ("Impact Size", Float) = 0.0
		_ImpactColor ("Impact Colour", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			float3 worldPos ;
		};

		half _Glossiness;
		half _ImpactSize ;
		fixed4 _Color;

		static const int _MAX_POINTS = 50 ;
		int _PointsCount ;
		fixed4 _Points [_MAX_POINTS] ;
		fixed4 _ImpactColor ;


		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color ;
			fixed emissive = 0 ;

			float3 pos = float4(IN.worldPos, 1).xyz ; // mul(unity_WorldToObject, float4(IN.worldPos, 1)).xyz ;

			for (int i = 0 ; i < min(_MAX_POINTS, _PointsCount) ; ++ i) {
				float dist = distance(_Points[i].xyz, pos.xyz);
				float size = _Points[i].w * _ImpactSize ;
				float curve = frac( 1.0 - max(0, size - dist) / _ImpactSize )  ;
				curve *= _ImpactSize * .1 * tan(curve) ;

				// emissive += curve * _Points[i].w ;
				emissive += curve * max(0, 1 - _Points[i].w) ;
			}

			o.Emission = emissive * _ImpactColor ;
			o.Albedo = c.rgb ;
			o.Metallic = 0 ;
			o.Smoothness = _Glossiness ;
			o.Alpha = c.a ;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
