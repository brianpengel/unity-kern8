﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using FlowEngine;

public class UIDirector : MonoBehaviour {

	[SerializeField] private ScriptedEvent onLogin ;
	[SerializeField] private UnityEvent afterLogin ;
	public string SessionID { get; private set ; }

	void Awake () {
		onLogin.AddListener<string, string> (OnLogin) ;
	}

	private void OnLogin(string res, string err) {
		if(System.String.IsNullOrEmpty(err)) {
			SessionID = res ;
			afterLogin.Invoke () ;
		}

		else Debug.LogWarning(err) ;
	}


}
